"""
WSGI config for MyCalDAVServer project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.6/howto/deployment/wsgi/
"""

import os
import sys


BASE_DIR = os.path.dirname(__file__)
sys.path.append(BASE_DIR)
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "MyCalDAVServer.settings")
from django.core.management import execute_from_command_line

try:
    execute_from_command_line(['manage.py', 'syncdb'])
except Exception, e:
    print str(e)

try:
    execute_from_command_line(['manage.py', 'collectstatic'])
except Exception, e:
    print str(e)
    
try:
    from django.contrib.auth.models import User

    User.objects.create_superuser(username='admin', email='admin@163.com', password='admin')
except Exception, e:
    print str(e)
from django.core.wsgi import get_wsgi_application

application = get_wsgi_application()
